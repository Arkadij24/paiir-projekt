/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommunicationModels;

import java.io.Serializable;

/**
 *
 * @author Kisacz Laptop
 */
public class ResponseMsg extends Msg implements Serializable{
    public String SourceIP; 
    
    public ResponseMsg(String type, String ip, String sourceIp) {
        super(type, ip);
        this.SourceIP = sourceIp; 
    }
    
}
