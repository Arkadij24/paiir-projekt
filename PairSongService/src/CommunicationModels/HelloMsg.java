/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommunicationModels;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Kisacz Laptop
 */
public class HelloMsg extends Msg implements Serializable{
    
    public ArrayList<String> SongTitles = new ArrayList<String>(); 
    
    public HelloMsg(String type, String ip, ArrayList<String> songTitles) {
        super(type, ip);
        this.SongTitles = songTitles; 
    }
    
}
