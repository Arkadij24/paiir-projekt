/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pairsongservice;

import Models.SongStore;
import java.util.ArrayList;

/**
 *
 * @author Kisacz Laptop
 */
public class DataStorage {
    public ArrayList<SongStore> KeptSongs = new ArrayList<SongStore>(); 
    
    public ArrayList<String> getSongs()
    {
        ArrayList<String> songs = new ArrayList<String>(); 
        
        for(int i =0; i < KeptSongs.size(); i++)
        {
            songs.add(KeptSongs.get(i).Title);
        }
        
        return songs;  
    }
    
    public void addSongs( ArrayList<String> songs, String ip)
    {
        System.out.println("Otrzymano piosenek od klienta: " + songs.size());   
        for(int i = 0; i < songs.size(); i++)
        {
            boolean contains = false; 
            int position = 0; 
            String songtitle = songs.get(i); 
            
            for(int j =0; j < KeptSongs.size(); j++)
            {
                contains = KeptSongs.get(j).Title.equals(songtitle);  
                if(contains) position = j; 
            }
            
            if(!contains)
            {
                System.out.println("dodaje piosenke = " + songtitle);
                SongStore nowy = new SongStore(songtitle); 
                nowy.SourceAdress.add(ip);
                KeptSongs.add(nowy);
            }
            else
            {
                KeptSongs.get(position).SourceAdress.add(ip); 
            }
        }       
        System.out.println("Obecnie jest piosenek: " + KeptSongs.size());
        for(int i = 0; i < KeptSongs.size(); i++)
        {
            System.out.println(KeptSongs.get(i).Title);
        }
    }
    public String getSongSource(String title)
        {
            System.out.println("Szukam piosenki");
            
            String adress = "null"; 
            boolean contains = false; 
            int position = -1; 
            
            for(int j =0; j < KeptSongs.size(); j++)
            {
                contains = KeptSongs.get(j).Title.equals(title);  
                if(contains) 
                {
                    position = j;
                    adress = KeptSongs.get(position).SourceAdress.get(0);
                    break;
                } 
            }
            System.out.println("Zwracam adres: " + adress);
           return adress; 
        }
    
    public void cleanIP(String ip)
    {
        for(int j =0; j < KeptSongs.size(); j++)
            {
                KeptSongs.get(j).SourceAdress.remove(ip);
            }
    }
}