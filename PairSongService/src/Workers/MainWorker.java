/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workers;

import CommunicationModels.HelloMsg;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import pairsongservice.DataStorage;

/**
 *
 * @author Kisacz Laptop
 */
public class MainWorker extends Thread{
    
    private Socket socket; 
    private DataStorage DataSorage = new DataStorage(); 
    public static ArrayList<ClientWorker> clientThreads = new ArrayList<ClientWorker>(); 
    
    @Override
    public void run()
    {
        try {
            ServerSocket server = new ServerSocket(2050);
            while(true)
            {
            socket = server.accept(); 
            System.out.println("Klient połączony!");
                         
            //Odczytanie przywitania
            //
            ObjectInputStream message = new ObjectInputStream(socket.getInputStream());
            HelloMsg msg = (HelloMsg) message.readObject(); 
            System.out.println("Otrzymano wiadomość " + msg.MsgType + " ");
            for(String string : msg.SongTitles)
            {
                System.out.println(string);
            }
            System.out.println(msg.SongTitles.size()); 
            
            //Oddanie listy obecnie przechowywanych piosenek
            ObjectOutputStream outMessage = new ObjectOutputStream(socket.getOutputStream());
            HelloMsg resp = new HelloMsg("Response", socket.getLocalAddress().getHostAddress(), DataSorage.getSongs());
            outMessage.writeObject(resp);
            outMessage.flush();
            
            //Dodanie nowych utworów do biblioteki
            DataSorage.addSongs(msg.SongTitles, socket.getInetAddress().getHostAddress());  
            
            System.out.println("Broatcastuje update");
            Alive();
            
            ClientWorker clientWorker = new ClientWorker(socket, DataSorage);
            clientThreads.add(clientWorker); 
            }
        } catch (IOException ex) {
            Logger.getLogger(MainWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void Alive() throws IOException
    {
        for(int i = 0; i < clientThreads.size(); i++)
        {
            clientThreads.get(i).updateFiles();
        }
    }
}