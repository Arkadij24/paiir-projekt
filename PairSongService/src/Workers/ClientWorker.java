/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workers;

import CommunicationModels.HelloMsg;
import CommunicationModels.ReqestMsq;
import CommunicationModels.ResponseMsg;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pairsongservice.DataStorage;

/**
 *
 * @author Kisacz Laptop
 */
public class ClientWorker extends Thread{
    public String ip; 
    public Socket socket;
    private InputStream in;
    private OutputStream out;
    DataStorage dataStorage; 
    
    public ClientWorker(Socket socket, DataStorage storage)
    {
        this.socket = socket;
        this.dataStorage = storage; 
        
        try
        {
            this.in = socket.getInputStream();
            this.out = socket.getOutputStream();
        } 
        catch (IOException ex)
        {
            Logger.getLogger(ClientWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        start();
        System.out.println("  Wystartowano wątek");
    }
    
    @Override
    public void run()
    {
            while(true) 
            {
                try {
                    System.out.println(socket.getInetAddress());
                    ObjectInputStream message = new ObjectInputStream(socket.getInputStream());
                    ReqestMsq msg = (ReqestMsq) message.readObject(); 
                    String sourceIp = dataStorage.getSongSource(msg.SongTitle);
                    System.out.println("Znaleziono piosenke: " + sourceIp);
                    
                    ObjectOutputStream outMessage = new ObjectOutputStream(socket.getOutputStream()); 
                    ResponseMsg resp = new ResponseMsg("Source", ip, sourceIp);
                    outMessage.writeObject(resp);
                    outMessage.flush();
                    
                } catch (SocketException ex) {
                    MainWorker.clientThreads.remove(this);
                    dataStorage.cleanIP(socket.getInetAddress().getHostAddress());
                    System.out.println("Rozłączono klienta");
                    break;
                } catch (IOException ex) {
                    Logger.getLogger(ClientWorker.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ClientWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
    }
    
    public void updateFiles() throws IOException
    {
                    ObjectOutputStream outMessage = new ObjectOutputStream(socket.getOutputStream()); 
                    HelloMsg resp = new HelloMsg("Update", ip, dataStorage.getSongs());
                    outMessage.writeObject(resp);
                    outMessage.flush();   
    }
}
