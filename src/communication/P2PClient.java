/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.Thread.sleep;
import java.net.Socket;

/**
 *
 * @author Arek
 */
public class P2PClient
{    
    public File download(String serverName, int port, String song)
    {
        File file = null;
        try
        {
            Socket socket = new Socket(serverName, port);
            System.out.println("Connected To " + serverName + " on " + port);
            System.out.println(socket);
            
            //sleep(500);
            
            OutputStream out = socket.getOutputStream();
            out.write(("getSong:" + song + ":").getBytes());
            out.write("\r\n".getBytes());
            
            file = new File(song);
            //file.createNewFile();

            InputStream in = socket.getInputStream();
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream fileOut = new BufferedOutputStream(fos);

            byte[] buffer = new byte[16384];

            int byteread;
            while ((byteread = in.read(buffer, 0, buffer.length)) != -1)
            {
                fileOut.write(buffer, 0, byteread);
            }
            
            fileOut.flush();
            
            fileOut.close();
            fos.close();
            in.close();
            socket.close();
            // start wątku klienta
            //newClientThread = new P2PClientThread(socket, song);
            
            //newClientThread.send(("getSong:" + song).getBytes());
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return file;
    }
}
