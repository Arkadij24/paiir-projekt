/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arek
 */
public class P2PServerThread extends Thread
{
    private Socket socket;
    private InputStream in;
    private OutputStream out;

    public P2PServerThread(Socket socket)
    {
        this.socket = socket;
        try
        {
            this.in = socket.getInputStream();
            this.out = socket.getOutputStream();
        } 
        catch (IOException ex)
        {
            Logger.getLogger(P2PServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        start();
    }
    
    @Override
    public void run()
    {
        try 
        {
            while(true) 
            {
                //odczytanie wiadomości wysłanej przez klienta
                int k = 0;
                StringBuffer sb = new StringBuffer();
                while((k = in.read()) != -1 && k != '\n') 
                {
                    sb.append((char) k);
                }
                
                //tokenizacja wiadomości na polecenie i argumenty wg znaku rozdzielającego ":"
                StringTokenizer st = new StringTokenizer(sb.toString(),":");
                
                System.out.println("odebrana wiadomosc: " + sb.toString());
                
                // rozpoznanie i zareagowanie na konkretne polecenia i argumentów
                switch(st.nextToken())
                {
                    // client
                    case "getSong":
                    {
                        String song = st.nextToken();
                        
                        File file = new File(song);
                        byte[] buffer = new byte[(int)file.length()];

                        FileInputStream fis = new FileInputStream(file);
                        BufferedInputStream in = new BufferedInputStream(fis);

                        in.read(buffer,0,buffer.length);
                        OutputStream out = socket.getOutputStream();

                        System.out.println("Sending file");
                        out.write(buffer,0, buffer.length);
                        out.flush();
                        out.close();
                        in.close();
                        System.out.println("File sended");
                    }
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
