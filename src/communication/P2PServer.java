/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arek
 */
public class P2PServer extends Thread
{
    private Socket socket;
    
    public P2PServer()
    {
        start();
    }
    
    @Override
    public void run()
    {
        try 
        {
            ServerSocket server = new ServerSocket(2010);
            
            while (true)
            {
                //Akceptacja polaczenia;
                socket = server.accept();
                System.out.println("Klient połączony!");
                
                //Tworzenie watku obsugujacego klienta
                P2PServerThread thread = new P2PServerThread(socket);
                //IDvector.addElement(IDcount);
                
                //dodawanie watku do wektora klientow
                //ClientVector.addElement(thread);
                //ClientVector.elementAt(IDvector.indexOf(IDcount)).send(("ID:" + IDcount + ":").getBytes());
                //serverGUI.logAdd("wysłano: " + "ID:" + IDcount + ":");
                //IDcount++;
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(P2PServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
