/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import CommunicationModels.HelloMsg;
import communication.Song;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import spiewaj.FiltrPiosenek;

/**
 *
 * @author Arek
 */
public abstract class Utils
{
    
    public static List<Song> downloadSongList(Socket serverConn) throws IOException, ClassNotFoundException // return List<Song> ??? void ???
    {
        //TODO
        //TestList
        ArrayList<String> localSongs = MapSongs(); 
        
        ObjectOutputStream outMessage = new ObjectOutputStream(serverConn.getOutputStream()); 
        HelloMsg aliveMsg = new HelloMsg("Hello od klienta", serverConn.getLocalAddress().toString(), localSongs);
        outMessage.writeObject(aliveMsg);
        outMessage.flush();
        
        ObjectInputStream message = new ObjectInputStream(serverConn.getInputStream());
        HelloMsg welcomeMsg = (HelloMsg) message.readObject(); 
        System.out.println("Otrzymano " + welcomeMsg.SongTitles.size());
         
        List<Song> songList = new ArrayList<Song>();
        
        for (int i = 0; i < welcomeMsg.SongTitles.size(); i++) {
            Song nowa = new Song(); 
            nowa.setTitle(welcomeMsg.SongTitles.get(i));
            songList.add(nowa);
        }
        
        return songList;
    };
    
    public static void createDummySongFiles(List<Song> songList)
    {
        File file;
        for(Song song : songList)
        {
            try
            {
                file = new File(song.getTitle());
                file.createNewFile();
            } 
            catch (IOException ex)
            {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static ArrayList<String> MapSongs()
    {
        ArrayList<String> pliki = new ArrayList<String>();
        
        File dir = new File("./");
        
        if(dir.isDirectory()) {
            File[] files = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".svpiew");
                }});
            
            for(File file : files)
            {
                if(file.isFile())
                {
                    pliki.add(file.getName()); 
                    System.out.println(file.getName());
                }
            }
        }
        System.out.println("Wysyłam pisosenek:" + pliki.size());
        return pliki; 
    }
    
    public static void clearDummyFiles()
    {
        File dir = new File("./");
        
        if(dir.isDirectory()) {
            File[] files = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".svpiew");
                }});
            
            for(File file : files)
            {
                if(file.isFile() && file.length() <= 0)
                {
                    file.delete();
                }
            }
        }
    }
}
