/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommunicationModels;

import java.io.Serializable;

/**
 *
 * @author Kisacz Laptop
 */
public class Msg implements Serializable{
    public String MsgType; 
    public String FromWhom; 
    
    public Msg(String type, String ip)
    {
        this.MsgType = type;
        this.FromWhom = ip; 
    }
}
