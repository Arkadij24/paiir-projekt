/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

import communication.P2PServer;
import java.awt.event.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import utils.Utils;

/**
 *
 * @author Tomek
 */
public class ZaspiewajMiTo extends JPanel implements ActionListener 
{
    /**
     * przestrzen z danymi dla calego programu
    */
    final static WspolnaPrzestrzen wp = new WspolnaPrzestrzen();
    public static Socket serverConnection; 
    
    /** 
     * konstruktor wygledu ekranu
    */
    public ZaspiewajMiTo() 
    {
        super(new GridBagLayout());
        // inicjalizacja komponentow 
        wp.tospiewasz = new JTextField(40);

        wp.tospiewasz.setToolTipText(Systemik.PLKtoJaaWindows("tu pojawiaje sie linijki do spiewania"));

        wp.tospiewasz.setFont(new Font("Default", Font.PLAIN, 32));

        wp.tospiewasz.setEditable(false);

        wp.naSylaby = new JTextField(40);

        wp.naSylaby.setToolTipText(Systemik.PLKtoJaaWindows("tu pojawiaje sie sylaby do spiewania"));

        wp.naSylaby.setFont(new Font("Default", Font.PLAIN, 32));

        wp.naSylaby.setEditable(false);

        wp.naSylaby.setBackground(Color.LIGHT_GRAY);

        wp.utwor = new JTextField(40);

        wp.utwor.addActionListener(this);

        wp.utwor.setBackground(Color.YELLOW);

        wp.utwor.setToolTipText(Systemik.PLKtoJaaWindows("tu wpisz nazwe pliku z utworem do odtwarzania i wcisnij ENTER"));

        wp.toBylo = new JTextArea(5, 40);

        wp.toBylo.setEditable(false);

        wp.toBylo.setToolTipText(Systemik.PLKtoJaaWindows("tu pojawiaje sie linijki juz spiewane"));

        JScrollPane glownaPlanszaPrzewijana = new JScrollPane(wp.toBylo);

        UIManager.put("FileChooser.lookInLabelText", "Katalog");
        UIManager.put("FileChooser.acceptAllFileFilterText", "Wszystkie pliki");
        UIManager.put("FileChooser.cancelButtonText", "Ignoruj");
        UIManager.put("FileChooser.openButtonText", "Graj");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Rodzaj pliku");
        UIManager.put("FileChooser.fileNameLabelText", "Nazwa pliku");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Ignoruj");
        UIManager.put("FileChooser.openButtonToolTipText", "Dialog");
        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
        UIManager.put("FileChooser.listViewButtonAccessibleName", "Lista");
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Szczegoly");
        UIManager.put("FileChooser.detailsViewButtonAccessibleName", "Szczegoly");
        UIManager.put("FileChooser.upFolderToolTipText", "Nie zmieniaj katalogu");
        UIManager.put("FileChooser.upFolderAccessibleName", "Nie zmieniaj katalogu");
        UIManager.put("FileChooser.homeFolderToolTipText", "Nie zmieniaj katalogu");
        UIManager.put("FileChooser.homeFolderAccessibleName", "Nie zmieniaj katalogu");
        UIManager.put("FileChooser.fileNameHeaderText", "Nazwa");
        UIManager.put("FileChooser.fileSizeHeaderText", "Rozmiar pliku");
        UIManager.put("FileChooser.fileTypeHeaderText", "Rodzaj pliku");
        UIManager.put("FileChooser.fileDateHeaderText", "Data");
        UIManager.put("FileChooser.fileAttrHeaderText", "Atrybuty");
        UIManager.put("FileChooser.openDialogTitleText", "Dialog");

        /*
        UIManager.put("FileChooser.readOnly", Boolean.TRUE);
         */
        wp.wyborPliku = new JFileChooser(".");

        wp.wyborPliku.setFileSelectionMode(JFileChooser.FILES_ONLY);

        wp.wyborPliku.setFileFilter(new FiltrPiosenek());

        wp.wyborPliku.addActionListener(this);

        // wstawianie komponentow do glownej planszy 
        GridBagConstraints ograniczeniaGridu = new GridBagConstraints();

        ograniczeniaGridu.gridwidth = GridBagConstraints.REMAINDER;

        ograniczeniaGridu.fill = GridBagConstraints.BOTH;

        ograniczeniaGridu.weightx = 1.0;

        ograniczeniaGridu.weighty = 1.0;

        add(glownaPlanszaPrzewijana, ograniczeniaGridu);

        ograniczeniaGridu.fill = GridBagConstraints.HORIZONTAL;

        add(wp.tospiewasz, ograniczeniaGridu);

        ograniczeniaGridu.fill = GridBagConstraints.HORIZONTAL;

        add(wp.naSylaby, ograniczeniaGridu);

        ograniczeniaGridu.fill = GridBagConstraints.HORIZONTAL;

        add(wp.utwor, ograniczeniaGridu);

        add(wp.wyborPliku, ograniczeniaGridu);
    } // koniec konstruktora 
    
    /** 
     * obsluga uderzenia ENTER w polu utworu
    */ 
    public void actionPerformed(ActionEvent wydarzenie) 
    {       
        try 
        {
            if (wydarzenie.getSource().getClass().getName() == "javax.swing.JFileChooser") 
            {
                JFileChooser wk = (JFileChooser) wydarzenie.getSource(); 
                File biekat = new File(".");
                if (!wk.getCurrentDirectory().getCanonicalPath().equals(biekat.getCanonicalPath()))
                {
                    wp.utwor.setText( "Nie zmieniaj katalogu!!!!"); 

                    wk.setCurrentDirectory(biekat); 
                    return; 
                } 
                wp.utwor.setText(wp.wyborPliku.getSelectedFile().getName()); 
            }
        } 
        catch (IOException e) {}

        if  (wp.programGrajacy != null)
                wp.programGrajacy.utwor = null; // nie be`da` akceptowane nowe przerwania
        wp.programGrajacy = new KlasaDoGraniaZeSpiewem(wp); 
        Runnable programGrajavcy = wp.programGrajacy;
        Thread  wavtekGrajavcy = new Thread(programGrajavcy);  
        wavtekGrajavcy.start();     
    } // koniec metody

    /** Tworzenie interfejsu graficznego 
     * Metoda ta bedzie wolana specjalnym chwytem rekomendowanym dla bezpieczen`stwa wetkow 
     * - czyli z wetku event dispatch .
     * - aby nie mieszaly sie ze`dania okienek i muzyki
    */
    private static void zrobInterfejsGraficzny() 
    {
        //tworzenie i przygotowanie bazowego okna JFrame 
        JFrame oknoNaswiat = new JFrame("ZapiewajMito");
        oknoNaswiat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //dodaj glowne plansze  
        oknoNaswiat.add(new ZaspiewajMiTo());
        //wyswietl okno 
        oknoNaswiat.pack();
        oknoNaswiat.setVisible(true);
    }  // koniec metody
    
    
    
    /** 
     * aplikacja
    */
    public static void main(String[] args) 
    {
        //dispeczowanie dla bezpieczen`stwa watkow w interfejsie graficznym
        javax.swing.SwingUtilities.invokeLater(
        /* 
            UWAGA: tu tworzymy nienazwane klase o interfejsie Runnable 
        */
        new Runnable() 
        {   	
            @Override
            public void run() 
            {	
                try {	
                    Utils.clearDummyFiles();
                    zrobInterfejsGraficzny();
                    serverConnection = new Socket("192.168.43.230",2050);  
                    
                    Utils.createDummySongFiles(Utils.downloadSongList(serverConnection));
                    wp.wyborPliku.updateUI();
                    
                    P2PServer server = new P2PServer();
                    //UpdateWorker updateWorker = new UpdateWorker(serverConnection);
                    
                } catch (IOException ex) {
                    Logger.getLogger(ZaspiewajMiTo.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ZaspiewajMiTo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }); // koniec konstrukcji "poz`niejszego wywolania 
    } // koniec metody 
} // koniec glownej klasy 
