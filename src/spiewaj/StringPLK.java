/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

/**
 *
 * @author Tomek
 */
class StringPLK extends StringPolski
{

    /**
     * Nazwa kodu znakow.      *
     * W miare moznosci nalezy tu podac oficjalna nazwe kodu, stosowana do
     * okreslenia charset w meta-instrukcjach HTML
     *
     * <META HTTP-EQUIV="content-type" CONTENT="text/html; CHARSET=.....">.      *
     */
    public String nazwaKodu()
    {
        return "PLK";
    }

    /**
     * lista kodow
     */
    private static String[] kodPLK =
    {
        "ą", "Ą",
        "ć", "Ć",
        "ę", "Ę",
        "ł", "Ł",
        "ń", "Ń",
        "ó", "Ó",
        "ś", "Ś",
        "ź", "Ź" //   ,"z|"   ,"Z|" - deprecated version 
        ,
         "ż", "Ż"
    };

    /**
     * zwraca referencje do listy kodow
     */
    protected String[] dajMojKod()
    {
        return kodPLK;
    }

    /**
     * zwraca referencje do oficjalnego kodu PLK
     */
    public static String[] dajOficjalnyKod()
    {
        return kodPLK;
    }

    // konstruktory
    /**
     * Konstruktor bazowy bezparanmetryczny
     */
    public StringPLK()
    {
        mojString = new String();
    }

    /**
     * Konstruktor pobierajacy String w kodzie PLK lub w niniejszym kodzie
     */
    public StringPLK(String wPLK)
    {
        mojString = zPLK(wPLK);
    }

    /**
     * Konstruktor konwertujacy na niniejszy kod z dowolnego polskiego kodu
     */
    public StringPLK(StringPolski dowolny)
    {
        mojString = zPLK(dowolny.naPLK());
    }

    /**
     * zwrca nowy obiekt w niniejszym kodzie z zamiana niniejszego tekstu na
     * male litery
     */
    public StringPLK naMale()
    {
        StringPLK x = new StringPLK(mojString);
        x.zmienNaMale();
        return x;
    }

    /**
     * zwrca nowy obiekt w niniejszym kodzie z zamiana niniejszego tekstu na
     * duze litery
     */
    public StringPLK naDuze()
    {
        StringPLK x = new StringPLK(mojString);
        x.zmienNaDuze();
        return x;
    }
}
