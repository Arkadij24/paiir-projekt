/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

/**
 *
 * @author Tomek
 */
public class Podzial
{

    public String[] tekst = new String[128];

    public String melodia = null, reszta = null;

    public int ile = 0;

    /**
     * bez tego pustego konstruktora Java zrzedzi
     */
    public Podzial(Podzial sp)
    {

    }

    /**
     * tu faktycznie wydziela sie z tekstu piosenki slowa pozostawiajec nuty
     * oraz znaczniki, w ktorych miejscach strofe trzeba zaspiewac popelnia sie
     * przy tym glupie zalozenie, ze zwrotka ma mniej niz FINITO77 strof
     *
     */
    public Podzial(String trescv0)
    {
        ile = 0;

        if (trescv0 == null)
        {
            return;
        }

        String trescv = trescv0;

        tekst[WspolnaPrzestrzen.FINITO77] = "_FINISZ_";

        melodia = "";

        int poznz = trescv.indexOf("=[:nowazwrotka]");

        if (poznz >= 0)
        {
            reszta = trescv.substring(poznz + 15);

            trescv = trescv.substring(0, poznz);
        }

        while (trescv.length() > 0)
        {
            int pozt = trescv.indexOf("=[");

            if (pozt < 0)
            {
                melodia += trescv /*+  " V0 X[NON_REGISTERED]="+WspolnaPrzestrzen.FINITO77 */ + "  V0  ";
                tekst[ile] = "";
                return;
            }

            melodia += trescv.substring(0, pozt) + "  V0  X[NON_REGISTERED]=" + (ile) + "  V0  ";

            trescv = trescv.substring(pozt + 2);

            int pozm = trescv.indexOf("]");

            if (pozm < 0)
            {
                return;
            }

            tekst[ile] = trescv.substring(0, pozm);

            trescv = trescv.substring(pozm + 1);

            ile++;
        }
    } // koniec konstruktora 
}// koniec 
