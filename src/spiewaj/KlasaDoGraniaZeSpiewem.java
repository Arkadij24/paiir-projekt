/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

import CommunicationModels.ReqestMsq;
import CommunicationModels.ResponseMsg;
import communication.P2PClient;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import org.jfugue.*;


/**
 *
 * @author Tomek
 */
public class KlasaDoGraniaZeSpiewem implements Runnable
{
    final WspolnaPrzestrzen wp;  

    final KlasaDoGraniaZeSpiewem taKlasa = this; 

    public String utwor;  // nazwa pliku z utweorem wyluskana z odpowieniego pola

    private final static String newline = "\n";

    public Player odtwarzacz = null; // aktualnie aktywny odtwarzacz
    

    /** 
     * konstruktor - przejmuje adres do wspolnej porzestrzeni danych
    */
    public KlasaDoGraniaZeSpiewem(WspolnaPrzestrzen wpIn) 

    {  

        wp = wpIn;

        utwor = wp.utwor.getText(); 

    } 


    /** 
     * oczyszczenie piosenki z komentarzy 
    */ 

    private String usunKomentarze(String piosnka)
    { 

        String reszta = piosnka;

        String poczatek = "";

        int poznz;

        while ((poznz = reszta.indexOf("![")) >= 0) 
        {

            poczatek += reszta.substring(0, poznz);

            reszta = reszta.substring(poznz + 2);

            poznz = reszta.indexOf("]");

            if (poznz >= 0) 
            {
                reszta = reszta.substring(poznz + 1);
            }

            else 
            {
                reszta = "";
            } 

        }

        poczatek += reszta; 

        return poczatek; 
    } 



    /** 
     * traktowanie powtorzen  
    */ 

    private String wyprostujPowtorki(String piosnka)
    { // powtorka: ||: ..nuty do powtorzenia - wszystkie pieciolinie ... |:| ew. 1 zakonczenie.... :|| 

        String reszta = piosnka;

        String repetycja = null, repetycja2 = null, repetycja2reszta;

        String pzak = null;

        String poczatek = "";

        int poznz;

        int pozro; 

        while ((poznz = reszta.indexOf("||:")) >= 0) 
        {

            poczatek += reszta.substring(0, poznz);

            reszta = reszta.substring(poznz + 3);

            poznz = reszta.indexOf(":||");

            if (poznz >= 0)
            { 
                repetycja=reszta.substring(0,poznz );

                reszta= reszta.substring(poznz+1 );
            }
            else 
            { 
                repetycja = reszta;
                reszta = ""; 
            }

            poznz=repetycja.indexOf("|:|");

            if (poznz >= 0) 
            { 
                pzak = repetycja.substring(poznz + 3);

                repetycja = repetycja.substring(0, poznz);
            }
            else 
            { 
                pzak = ""; 
            }

            repetycja2 = "";

            repetycja2reszta = new String(repetycja); 

            while ((pozro = repetycja2reszta.indexOf("=[")) >= 0 && (poznz = reszta.indexOf("=[")) >= 0) 
            {
                // pozro - do podstaw. w repetycji, poznz - co podstawic

                repetycja2 += repetycja2reszta.substring(0,pozro );

                repetycja2reszta=repetycja2reszta.substring(pozro );

                reszta= reszta.substring(poznz );

                pozro=repetycja2reszta.indexOf("]");

                poznz=reszta.indexOf("]");

                if (poznz >= 0 && pozro >= 0)
                { 
                    repetycja2 += reszta.substring(0, poznz + 1);

                    reszta = reszta.substring(poznz + 1);

                    repetycja2reszta = repetycja2reszta.substring(pozro + 1);
                }
            }

            repetycja2 += repetycja2reszta;

            // teraz podstawianie stringow 
            poczatek += " ||||| " + repetycja + " ||| " + pzak + " ||| " + repetycja2 + " ||||| ";

        }

        poczatek += reszta;

        System.out.println("powtorki: " + poczatek + ";"); // test

        return poczatek; 

    } 


    /** 
     * faktyczne wykonanie zadania
    */
    
    private P2PClient client = new P2PClient();

    public void run() 
    {  
        try 
        { 
            // to miejsce trzeba zmienic, aby czytac z internetu

            File plikUchw = new File(utwor);
            
            if(plikUchw.length() <= 0)
            {
                System.out.println("PLIK PUSTY TRZEBA POBRAC");
                System.out.println("utwor = " + utwor);
                
                Socket local = ZaspiewajMiTo.serverConnection;
                                System.out.println("Test request");
                ObjectOutputStream outMsg = new ObjectOutputStream(ZaspiewajMiTo.serverConnection.getOutputStream());
                System.out.println("Szykuje request");
                ReqestMsq req = new ReqestMsq("Request", local.getInetAddress().getHostAddress(), plikUchw.getName());
                outMsg.writeObject(req);
                outMsg.flush();
                
                System.out.println("Wysłano prośbe");
                
                ObjectInputStream inMsg = new ObjectInputStream(ZaspiewajMiTo.serverConnection.getInputStream());
                ResponseMsg resp = (ResponseMsg) inMsg.readObject(); 
                
                System.out.println("Otrzymano adres źródła: " + resp.SourceIP);
                
                plikUchw = client.download(resp.SourceIP, 2010, utwor);
            }

            StringBuffer buforCzytanegoTekstuPiosnki = new StringBuffer();

            BufferedReader czytnik = null;

            try 
            {
                czytnik = new BufferedReader(new FileReader(plikUchw));

                String text = null;

                // wczytanie calosci

                while ((text = czytnik.readLine()) != null)	
                {
                    buforCzytanegoTekstuPiosnki.append(text).append(" " + System.getProperty("line.separator"));
                }
            } 
            catch (FileNotFoundException e) 
            {
                e.printStackTrace();
            } 
            catch (IOException e) 
            {
                e.printStackTrace();
            } 
            finally
            {
                try 
                {
                    if (czytnik != null)  
                    {	
                        czytnik.close();
                    }
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }

            String piosnka = buforCzytanegoTekstuPiosnki.toString();

            final boolean revcznesylabizowanie = (piosnka.indexOf("![:sylabyreczne]") >= 0);

            piosnka = wyprostujPowtorki(usunKomentarze(piosnka));

            final Podzial podzial = new Podzial(piosnka);

            wp.toBylo.setText("\n");

            wp.toBylo.setCaretPosition(wp.toBylo.getDocument().getLength());

            wp.toBylo.repaint();

            String text;

            wp.toBylo.setText("PIOSNKA " + utwor + "\n");
            
            // pierwszy warunek: nikt nie przerwal odtwarzania
            // drugi warunek: jeszcze se zwrotki
            while (wp.programGrajacy == taKlasa && podzial.melodia != null)
            { 
                odtwarzacz = new Player();

                // przygotowanie do dzialania tzw. przewidywacza JFugue  

                Anticipator przewidywacz = new Anticipator();

                // uwaga: znowu anonimowa klasa, dosc duza
                przewidywacz.addParserListener(new EasyAnticipatorListener()   // (w tym miejscu mozna by uzyc wlasny ParserListener, ale po co?)
                { 	
                    private int najnowszyWiersz = 0;

                    private int najnowszaSylaba = 0;

                    private long poprzedniaChwila = -10;

                    private String[] sylaby = null;  // strofa podzielona na sylaby 

                    /** 
                     * obsluga  zdarzenia zauwazenia nuty w odtwarzaczu 
                    */
                    public void extendedNoteEvent(Voice voice, Instrument instrument, Note note)
                    {      
                        if (wp.programGrajacy != taKlasa)
                        {	 
                            odtwarzacz.allNotesOff(); // zatrzymanie odtwarzacza

                            //System.out.println(" oo"+(odtwarzacz==null));

                            odtwarzacz.close(); 

                            //wp.odtwarzacz = null;
                        }

                        if 
                        (chwila > poprzedniaChwila && sylaby != null && najnowszaSylaba < sylaby.length && sylaby[najnowszaSylaba] != null

                            && voice.getVoice() == 0 && note != null && note.getDuration() > 0 && !note.isEndOfTie() && !note.isRest()

                            && wp.programGrajacy == taKlasa) 		
                        {
                            wp.naSylaby.setText(wp.naSylaby.getText()+Systemik.PLKtoJaaWindows( sylaby[ najnowszaSylaba ]  ));

                            System.out.print(sylaby[ najnowszaSylaba++ ]); 
                            //" "+note.getValue()

                            //+"/"+note.getDuration()

                            //"*"+note.getMusicString() // +"/"+note. getType()

                            //+"*"+voice.getVoice()

                            poprzedniaChwila=chwila;  
                        }	
                    } // koniec metody 

                    /** metoda stwierdza, czy znak jest samogloske
                     * dziala tylko w notacji PLK
                    */
                    private boolean czySamogloska(char x)
                    {
                        x=Character.toLowerCase(x); 

                        return (x == 'a'
                                || x == 'e'
                                || x == 'i'
                                || x == 'o'
                                || x == 'u'
                                || x == 'y');
                    } // koniec metody

                    /** metoda dzieli strofe na sylaby automatycznie
                     * dziala tylko w notacji PLK
                    */
                    private String [] podzielNaSylaby(String zdanie)
                    {
                        // policz samogloski 

                        int ileSylab = 0;

                        for (int j = 0; zdanie != null && j < zdanie.length(); j++) 
                        {
                            if (czySamogloska(zdanie.charAt(j))) 
                            {
                                ileSylab++;
                            }
                        }

                        String[] sylaby = new String[ileSylab + 2];

                        // a teraz podziel na sylaby 
                        //System.out.println("-"+zdanie+"- sylab: "+ileSylab);
                        if (ileSylab == 0)
                        {	
                            sylaby[0] = new String("?");

                            sylaby[1] = new String("?");

                            return sylaby;
                        } 

                        int ktoraSylaba = 0;

                        boolean bylaSpacja = false;

                        sylaby[ktoraSylaba]=new String(""); 	

                        //Tomek: ta sekcja kodu jest wybitnie spierdolona, boję się cokolwiek ruszać
                        //Arek: poprawilem formatowanie bo mnie chuj strzela od losowych spacji i wcięć oraz braku spacji miedzy operatorami
                        // teraz to wygląda w miarę po ludzku
                        for (int j = 0; j < zdanie.length(); j++) 
                        {
                            sylaby[ktoraSylaba] += zdanie.charAt(j);

                            if (!bylaSpacja) 
                            {
                                if (' ' == zdanie.charAt(j)) 
                                {
                                    if (ktoraSylaba > 0) 
                                    {
                                        sylaby[ktoraSylaba - 1] += sylaby[ktoraSylaba];

                                        sylaby[ktoraSylaba] = new String("");

                                        bylaSpacja = true;
                                    }
                                }
                            }

                            if (czySamogloska(zdanie.charAt(j))) 
                            {
                                if (zdanie.charAt(j) != 'i' || j == zdanie.length() - 1 || !czySamogloska(zdanie.charAt(j + 1))) 
                                {
                                    if (j < zdanie.length() - 1 && zdanie.charAt(j + 1) == '`') 
                                    {
                                        sylaby[ktoraSylaba] += zdanie.charAt(j + 1);
                                        j++;
                                    }

                                    ktoraSylaba++;

                                    bylaSpacja = false;

                                    sylaby[ktoraSylaba] = new String("");
                                }
                            }
                        }

                        if (ktoraSylaba > 0)
                        {
                            sylaby[ktoraSylaba - 1] += sylaby[ktoraSylaba];

                            sylaby[ktoraSylaba] = null;	
                        }

                        return sylaby;

                    } // koniec metody


                    /** metoda dzieli strofe na sylaby w oparciu o wskazania reczne, czyli - 
                     * dziala tylko w notacji PLK
                    */
                    private String [] podzielRevcznieNaSylaby(String zdanie)
                    { 
                        // policz spacje i myslniki 

                        int ileSylab = 0;

                        for (int j = 0; zdanie != null && j < zdanie.length(); j++) 
                        {
                            if ('-' == zdanie.charAt(j)) 
                            {
                                ileSylab++;
                            } 
                            else if (' ' == zdanie.charAt(j)) 
                            {
                                ileSylab++;
                            }
                        }

                        String[] sylaby = new String[ileSylab + 2];

                        // a teraz podziel na sylaby  

                        if (ileSylab == 0) 
                        { 
                            sylaby[0] = new String("?");

                            sylaby[1] = new String("?");

                            return sylaby;
                        } 

                        int ktoraSylaba = 0;

                        boolean bylaSamogloska = false;

                        sylaby[ktoraSylaba] = new String("");

                        for (int j = 0; j < zdanie.length(); j++) 
                        {
                            sylaby[ktoraSylaba] += zdanie.charAt(j);

                            if ('-' == zdanie.charAt(j) || (bylaSamogloska && ' ' == zdanie.charAt(j))) 
                            {
                                sylaby[++ktoraSylaba] = new String("");

                                bylaSamogloska = false;
                            } 
                            else if (czySamogloska(zdanie.charAt(j))) 
                            {
                                bylaSamogloska = true;
                            }
                        }

                        return sylaby;

                    } // koniec metody

                    /** 
                     * metoda przechwytuje zdarzenia zauwazenia przez odtwarzacz miejsca, w ktorym wstawiono znacznik nowej strofy zwrotki
                    */
                    public void controllerEvent(Controller controller)
                    {
                        if (controller.getIndex() != Controller.NON_REGISTERED_COARSE) 
                        {
                            return;
                        } 

                        if (wp.programGrajacy != taKlasa) 
                        {
                            return;
                        }

                        String text;

                        text = wp.tospiewasz.getText();

                        wp.toBylo.append(text + "\n");

                        wp.toBylo.setCaretPosition(wp.toBylo.getDocument().getLength());

                        wp.toBylo.repaint();

                        najnowszyWiersz = controller.getValue();

                        najnowszaSylaba = 0;

                        wp.tospiewasz.setText(Systemik.PLKtoJaaWindows(podzial.tekst[najnowszyWiersz]));

                        wp.tospiewasz.repaint();

                        //Arek: skrócony zapis ifa - warunek ? jeśli_true : else
                        sylaby = (revcznesylabizowanie
                                        ? podzielRevcznieNaSylaby(podzial.tekst[najnowszyWiersz]) 
                                        : podzielNaSylaby(podzial.tekst[najnowszyWiersz]));

                        System.out.println(); // nowy wiersz na czarnym ekranie

                        poprzedniaChwila = chwila;

                        wp.naSylaby.setText(Systemik.PLKtoJaaWindows(sylaby[najnowszaSylaba]));

                        System.out.print(sylaby[najnowszaSylaba++]);

                        //zapewniamy widocznosc tekstu 

                        // mimo braku selekcji stosownego pola tekstowego 

                        wp.tospiewasz.selectAll();

                    } // koniec metody 

                    private long chwila = -1;


                    /** 
                     * rejestrowanie melodiowego uplywu czasu od poczetku odtwarzania
                    */
                    public void timeEvent(Time time)
                    {	
                        chwila = time.getTime(); 
                    }

                    } // koniec anonimowej klasy EasyAnticipatorListener

                ); // koniec przewidywacz.addParserListener(


                // i znowu jestesmy w metodzie public void run() klasy  KlasaDoGraniaZeSpiewem 

                // odtwarzanie utworu; na 200 ms przed KAzDYM ZDARZENIEM POJAWI SIe SYGNAl - WYSlANY DO NASlUCHIWACZA 

                odtwarzacz.allNotesOff(); 

                // do zachowania midi odtwarzacz.saeMidi( wewnPostacvMelodii , new File("plikmidi."+utwor+".midi"));
                Pattern wewnPostacvMelodii = new Pattern(podzial.melodia);  

                if (wp.programGrajacy == taKlasa)    
                {
                    odtwarzacz.play(przewidywacz, wewnPostacvMelodii, 200); //<-- tu jest to 200 ms
                }

                while(wp.programGrajacy == taKlasa && !odtwarzacz.isFinished()) 
                {
                    System.out.print("*");
                } 

                if (wp.programGrajacy == taKlasa) 
                {
                    odtwarzacz.close();
                } 

                text = wp.tospiewasz.getText();

                wp.toBylo.append(text + "\n");

                wp.toBylo.setCaretPosition(wp.toBylo.getDocument().getLength());

                wp.toBylo.repaint();

                wp.tospiewasz.selectAll();

                if (podzial.reszta == null) 
                {
                    podzial.melodia = null;
                } 
                else
                {
                    Podzial podzialX = new Podzial(podzial.reszta);

                    podzial.reszta = podzialX.reszta;

                    podzial.tekst = podzialX.tekst;
                }
            } // koniec while 

            text="-- KONIEC --"; 

            wp.tospiewasz.setText(text);
            wp.tospiewasz.selectAll();
        } 
        
        catch(Exception ex) 
        { 
            ex.printStackTrace(); 
        }
    }  // koniec metody run 
}  // koniec klasy  
