/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

import java.io.File;
import javax.swing.ImageIcon;

/**
 *
 * @author Tomek
 */
public class ListaRozszerzen
{

    public final static String svpiew = "svpiew";
    public final static String song = "song";
    public final static String lied = "lied";

    /*
     * daj rozszerzenie pliku.
     */
    public static String dajRozszerzenie(File f)
    {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1)
        {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    /**
     * ImageIcon, lub null gdy bledna sciezkaif the path was inalid.
     */
    protected static ImageIcon createImageIcon(String path)
    {
        java.net.URL imgURL = ListaRozszerzen.class.getResource(path);
        if (imgURL != null)
        {
            return new ImageIcon(imgURL);
        } 
        else
        {
            System.err.println("Brak pliku : " + path);
            return null;
        }
    }
}
