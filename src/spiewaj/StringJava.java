/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

/**
 *
 * @author Tomek
 */
public class StringJava extends StringPolski
{

    public String nazwaKodu()
    {
        return "JavaOwnWindows-1250";
    }

    private static String[] kodJaa =
    {

        /* "ą", "Ą"
   ,"ć"   ,"Ć" 
   ,"ę"   ,"Ę" 
   ,"ł"   ,"Ł" 
   ,"ń"   ,"Ń" 
   ,"ó"   ,"Ó" 
   ,"ś"   ,"Ś" 
   ,"ź"   ,"Ź" 
   ,"ż"   ,"Ż" 
         */
        "" + (char) (5 + 256 * 1), "" + (char) (4 + 256 * 1),
         "" + (char) (7 + 256 * 1), "" + (char) (6 + 256 * 1),
         "" + (char) (25 + 256 * 1), "" + (char) (24 + 256 * 1),
         "" + (char) (66 + 256 * 1), "" + (char) (65 + 256 * 1),
         "" + (char) (68 + 256 * 1), "" + (char) (67 + 256 * 1),
         "" + (char) (243 + 256 * 0), "" + (char) (211 + 256 * 0),
         "" + (char) (91 + 256 * 1), "" + (char) (90 + 256 * 1),
         "" + (char) (122 + 256 * 1), "" + (char) (121 + 256 * 1),
         "" + (char) (124 + 256 * 1), "" + (char) (123 + 256 * 1)

    };

    protected String[] dajMojKod()
    {
        return kodJaa;
    }

    // konstruktory
    public StringJava()
    {
        mojString = new String();
    }

    public StringJava(String wPLK)
    {
        mojString = zPLK(wPLK);
    }

    public StringJava(StringPolski dowolny)
    {
        mojString = zPLK(dowolny.naPLK());
    }
}
