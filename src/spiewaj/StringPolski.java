/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

/**
 *
 * @author Tomek
 */
abstract class StringPolski
{

    protected String mojString;

    protected abstract String[] dajMojKod();

    /**
     * zwraca oficjalna nazwe kodu
     */
    public abstract String nazwaKodu();

    /**
     * zwraca instrukcje meta w HTML dla danego kodu
     */
    public String htmlMeta()
    {
        return "<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; CHARSET=" + nazwaKodu() + "\">";
    }

    /**
     * zmiana zawartosci polskiego stringu: zakladamy, ze parametr jest w PLK
     */
    public void zmienNa(String wPLK)
    {
        mojString = zPLK(wPLK);
    }

    /**
     * zmiana zawartosci polskiego stringu. rekodowanie automatyczne
     */
    public void zmienNa(StringPolski s)
    {
        mojString = zPLK(s.naPLK());
    }

    /**
     * zmiana zawartosci polskiego stringu na odpowiednik pisany duZymi literami
     */
    public void zmienNaDuze()
    {
        mojString = zPLK(naPLK().toUpperCase());
    }

    /**
     * zmiana zawartosci polskiego stringu na odpowiednik pisany malymi literami
     */
    public void zmienNaMale()
    {
        mojString = zPLK(naPLK().toLowerCase());
    }

    /**
     * porownanie dwu napisow w PLK. Zwraca 0, jesli sa rowne, liczbe ujemna,
     * jesli pierwszy poprzedza drugi, a dodatnia w pozostalym ptrzypadku
     */
    public static int porownajPLK(String s1, String s2)
    {
        int j;

        int d1 = s1.length(), d2 = s2.length();

        int d = ((d1 < d2) ? d1 : d2);

        for (j = 0; j < d && s1.charAt(j) == s2.charAt(j); j++);

        if (j == d)
        {
            return d1 - d2;
        }

        if (s1.charAt(j) == '`')
        {
            return 1;
        }

        if (s2.charAt(j) == '`')
        {
            return -1;
        }

        return ((int) s1.charAt(j)) - ((int) s2.charAt(j));
    }

    /**
     * porownanuie dwu napisow w PLK bez uwzglednienia wielkosci liter. Zwraca
     * 0, jesli sa rowne, liczbe ujemna, jesli pierwszy poprzedza drugi, a
     * dodatnia w pozostalym ptrzypadku
     */
    public static int porownajPLKBezDuzych(String s1, String s2)
    {
        return porownajPLK(s1.toUpperCase(), s2.toUpperCase());
    }

    /**
     * porownanuie dwu napisow w jezyku polskim. Zwraca 0, jesli sa rowne,
     * liczbe ujemna, jesli pierwszy poprzedza drugi, a dodatnia w pozostalym
     * ptrzypadku
     */
    public int porownaj(StringPolski p2)
    {
        String s1 = naPLK(), s2 = p2.naPLK();

        return porownajPLK(s1, s2);
    }

    /**
     * porownanuie dwu napisow w jezyku polskim bez uwzglednienia wielkosci
     * liter. Zwraca 0, jesli sa rowne, liczbe ujemna, jesli pierwszy poprzedza
     * drugi, a dodatnia w pozostalym ptrzypadku
     */
    public int porownajBezDuzych(StringPolski p2)
    {
        String s1 = naPLK(), s2 = p2.naPLK();

        return porownajPLKBezDuzych(s1, s2);
    }

    /**
     * Podanie kodu sterujacego ze StringuPolskiego. W wiekszosci takiego kodu
     * nie ma i wtedy zwraca sie null. Kody sterujace ma Unicode, UniBigEndian,
     * 16Unicode, UTF8
     */
    public String dajSterKodu()
    {
        return null;
    }

    /**
     * Usuwanie kodu sterujacego ze StringuPolskiego - w wiekszosci nic sie nie
     * robi
     */
    public void czyscSterKodu()
    {
        String ks;

        ks = dajSterKodu();

        if (ks == null)
        {
            return;
        }

        int j;

        while ((j = mojString.indexOf(ks)) > 0)
        {
            mojString = mojString.substring(0, j) + mojString.substring(j + ks.length());
        }
    }

    /** pobranie zawartosci mojStringu - czyli String w danym polskim kodzie */
    public String toString() 
    {
        return new String(mojString);
    }
    
    protected String naPLK(String innyNizPLK)
    {
        return zKoduNaKod(dajMojKod(),StringPLK.dajOficjalnyKod(), innyNizPLK); 
    }

    protected String naPLK(String[] zKodu, String innyNizPLK)
    {
        return zKoduNaKod(zKodu,StringPLK.dajOficjalnyKod(), innyNizPLK); 
    }

    /** zamiana zawartosci mojStringu na kod PLK */
    public String naPLK() 
    {
        return naPLK(mojString);
    }

    protected String zPLK(String wPLK)
    {
        return zKoduNaKod(StringPLK.dajOficjalnyKod(), dajMojKod(), wPLK); 
    }

    /** 
     * Zamiana z tablicy kodowej 1 na tablice kodowe 2 
    */
    protected String zKoduNaKod(String[] kod1, String[] kod2, String s1)
    {
        int j;
        int k;
        boolean znaleziono;

        String wynik = new String();

        for (j = 0; j < s1.length(); j++)
        {
            znaleziono = false;

            for (k = Math.min(kod1.length, kod2.length) - 1; k >= 0 && !znaleziono; k--)
            {
                if (kod1[k].length() <= s1.length() - j)
                {
                    if (kod1[k].equals(s1.substring(j, j + kod1[k].length())))
                    {
                        znaleziono = true;

                        wynik += kod2[k];

                        j += kod1[k].length() - 1;
                    }
                }
            }

            if (!znaleziono)
            {
                wynik += s1.charAt(j);
            }
        }

        return wynik;
    }
} // koniec klasy
