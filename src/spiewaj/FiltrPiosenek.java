/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

import java.io.File;

/**
 *
 * @author Tomek
 */
public class FiltrPiosenek extends javax.swing.filechooser.FileFilter
{
    //Tylko pliki piosenek spiew i song 
    @Override
    public boolean accept(File f) 
    {
        if (f.isDirectory()) 
        {
            return false; // nie akceptujemy katalogow
        }
 
        String rozszerzenie = ListaRozszerzen.dajRozszerzenie(f);
        if (rozszerzenie != null) 
        {
            if (rozszerzenie.equals(ListaRozszerzen.song) || rozszerzenie.equals(ListaRozszerzen.lied) || rozszerzenie.equals(ListaRozszerzen.svpiew))
	    {
                return true;
            }
            else 
            {
                return false;
            }
        }
        return false;
    }

    //Opis filtra 
    @Override
    public String getDescription() 
    {
        return "Tylko piosenki";
    }
}
