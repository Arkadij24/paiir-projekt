/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiewaj;

import CommunicationModels.HelloMsg;
import CommunicationModels.Msg;
import communication.Song;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Utils;

/**
 *
 * @author Kuba
 */
public class UpdateWorker extends Thread{
    
    Socket serverConn; 
    
    public UpdateWorker(Socket server)
    {
        serverConn = server; 
        start(); 
    }
    
    public void run()
    {
        ObjectInputStream message;
        try {
            message = new ObjectInputStream(serverConn.getInputStream());
            Msg test = (Msg) message.readObject(); 
            
            if(test.MsgType.equals("Update"))
            {
            HelloMsg welcomeMsg = (HelloMsg) test; 
            System.out.println("Otrzymano update: " + welcomeMsg.SongTitles.size());
            
            List<Song> songList = new ArrayList<Song>();
        
            for (int i = 0; i < welcomeMsg.SongTitles.size(); i++) {
                Song nowa = new Song(); 
                nowa.setTitle(welcomeMsg.SongTitles.get(i));
                songList.add(nowa);
            }
            Utils.createDummySongFiles(songList);
            }
        } catch (IOException ex) {
            Logger.getLogger(UpdateWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
